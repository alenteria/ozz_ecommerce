<?php
// Exit if accessed directly
if ( !defined('ABSPATH')) exit;

/* Add custom functions below */

add_filter('tc_credits_display', 'my_custom_credits');

function my_custom_credits(){ 
$credits = 'Credits to CSNA 2014';
$newline_credits = '';
return '<div class="span4 credits">
    		    	<p> &middot; &copy; '.esc_attr( date( 'Y' ) ).' <a href="'.esc_url( home_url() ).'" title="'.esc_attr(get_bloginfo()).'" rel="bookmark">'.esc_attr(get_bloginfo()).'</a> &middot; '.($credits ? $credits : 'Designed by <a href="http://www.themesandco.com/">Arnel &amp; Co</a>').' &middot;'.($newline_credits ? '<br />&middot; '.$newline_credits.' &middot;' : '').'</p>		</div>';
}

 