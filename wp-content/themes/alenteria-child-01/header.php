<?php if ( ! isset( $_SESSION ) ) session_start(); ?>
<!DOCTYPE html>
<!--[if IE 6]>
<html id="ie6" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 7]>
<html id="ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html id="ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 6) | !(IE 7) | !(IE 8)  ]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>" />
	<title><?php elegant_titles(); ?></title>
	<?php elegant_description(); ?>
	<?php elegant_keywords(); ?>
	<?php elegant_canonical(); ?>

	<?php do_action( 'et_head_meta' ); ?>

	<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />

	<?php $template_directory_uri = get_template_directory_uri(); ?>
	<!--[if lt IE 9]>
	<script src="<?php echo esc_url( $template_directory_uri . '/js/html5.js"' ); ?>" type="text/javascript"></script>
	<![endif]-->

	<script type="text/javascript">
		document.documentElement.className = 'js';
	</script>

	<?php wp_head(); ?>

	<script src="<?php echo esc_url( $template_directory_uri . '/js/alenteria_custom.js"' ); ?>" type="text/javascript"></script>
	<style type="text/css">
		@media (max-width: 1033px) {
		  #header {
		    display: none;
		  }
		  .et_fixed_nav #page-container{
		  	padding-top: 0px !important;
		  }
		  #ribbon{
		  	margin-bottom: -30px !important;
		  }
		}
		@media (min-width: 1033px) {
		 #main-header{
			display: none;  	 
		  }		   
		}
	</style>
</head>
<body <?php body_class(); ?>>
<?php
	if ( is_page_template( 'page-template-blank.php' ) ) {
		return;
	}

	$et_secondary_nav_items = et_divi_get_top_nav_items();

	$et_phone_number = $et_secondary_nav_items->phone_number;

	$et_email = $et_secondary_nav_items->email;

	$et_contact_info_defined = $et_secondary_nav_items->contact_info_defined;

	$show_header_social_icons = $et_secondary_nav_items->show_header_social_icons;

	$et_secondary_nav = $et_secondary_nav_items->secondary_nav;

	$primary_nav_class = 'et_nav_text_color_' . et_get_option( 'primary_nav_text_color', 'dark' );

	$secondary_nav_class = 'et_nav_text_color_' . et_get_option( 'secondary_nav_text_color', 'light' );

	$et_top_info_defined = $et_secondary_nav_items->top_info_defined;
?>

	<?php if ( $et_top_info_defined ) : ?>
		<div id="top-header" class="<?php echo esc_attr( $secondary_nav_class ); ?>">
			<div class="container clearfix">

			<?php if ( $et_contact_info_defined ) : ?>

				<div id="et-info">
				<?php if ( '' !== ( $et_phone_number = et_get_option( 'phone_number' ) ) ) : ?>
					<span id="et-info-phone"><?php echo esc_html( $et_phone_number ); ?></span>
				<?php endif; ?>

				<?php if ( '' !== ( $et_email = et_get_option( 'header_email' ) ) ) : ?>
					<span id="et-info-email"><?php echo esc_html( $et_email ); ?></span>
				<?php endif; ?>

				<?php
				if ( true === $show_header_social_icons ) {
					get_template_part( 'includes/social_icons', 'header' );
				} ?>
				</div> <!-- #et-info -->

			<?php endif; // true === $et_contact_info_defined ?>

				<div id="et-secondary-menu">
				<?php
					if ( ! $et_contact_info_defined && true === $show_header_social_icons ) {
						get_template_part( 'includes/social_icons', 'header' );
					} else if ( $et_contact_info_defined && true === $show_header_social_icons ) {
						ob_start();

						get_template_part( 'includes/social_icons', 'header' );

						$duplicate_social_icons = ob_get_contents();

						ob_end_clean();

						printf(
							'<div class="et_duplicate_social_icons">
								%1$s
							</div>',
							$duplicate_social_icons
						);
					}

					if ( '' !== $et_secondary_nav ) {
						echo $et_secondary_nav;
					}

					et_show_cart_total();
				?>
				</div> <!-- #et-secondary-menu -->

			</div> <!-- .container -->
		</div> <!-- #top-header -->
	<?php endif; // true ==== $et_top_info_defined ?>

	<?php
		$logo = ( $user_logo = et_get_option( 'divi_logo' ) ) && '' != $user_logo
			? $user_logo
			: $template_directory_uri . '/images/logo.png';
	?>

	<header id="header" class="custom-header">
    	<div class="wrapper"><a href="//www.saveabrand.com" tabindex="100" class="logo"><img src="<?php echo esc_attr( $logo ); ?>" alt="<?php echo esc_attr( get_bloginfo( 'name' ) ); ?>" width="117" height="19"></a>
        	<ul class="help-phone"> 
				<li class="tab phone_tab">(888) 401-4678</li>
				<li class="tab live_chat_tab">
					<a href="https://my.bluehost.com/hosting/chatlite" tabindex="101">
						<div class="svg_icon" data-color="white" data-icon="chat" style="background-image: none;">
							<object width="100%" height="100%">
								<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 800 800" style="enable-background:new 0 0 800 800;" xml:space="preserve">
    								<path d="M554.32,2.408c-135.685,0-245.679,87.053-245.679,194.438c0,63.975,39.214,120.556,99.465,155.991 c-5.892,32.308-24.658,70.37-50.896,96.609c64.063-4.781,120.447-23.151,157.366-60.932c12.968,1.674,26.18,2.77,39.745,2.77 c135.686,0,245.68-87.052,245.68-194.438C800,89.461,690.006,2.408,554.32,2.408z M439.154,235.234 c-21.201,0-38.388-17.187-38.388-38.389c0-21.201,17.187-38.389,38.388-38.389c21.202,0,38.389,17.188,38.389,38.389 C477.543,218.048,460.356,235.234,439.154,235.234z M554.32,235.234c-21.201,0-38.389-17.187-38.389-38.389 c0-21.201,17.188-38.389,38.389-38.389c21.202,0,38.389,17.188,38.389,38.389C592.709,218.048,575.522,235.234,554.32,235.234z M669.487,235.234c-21.202,0-38.389-17.187-38.389-38.389c0-21.201,17.187-38.389,38.389-38.389s38.389,17.188,38.389,38.389 C707.876,218.048,690.689,235.234,669.487,235.234z M506.637,644.275c-77.894-62.601-99.676-31.828-135.019,3.509 c-24.674,24.684-87.116-26.86-141.031-80.779c-53.919-53.925-105.453-116.357-80.781-141.035 c35.345-35.339,66.109-57.134,3.491-135.005C90.699,213.063,48.971,272.871,14.734,307.11 c-39.526,39.512-2.082,186.759,143.445,332.31C303.726,784.95,450.973,822.368,490.476,782.871 C524.707,748.63,584.532,706.906,506.637,644.275z M97.229,333.2c-1.051,0.43-2.593,1.061-4.555,1.864 c-3.714,1.423-8.155,3.033-13.096,5.183c-2.306,1.111-4.892,2.314-7.25,3.796c-2.445,1.46-4.876,3.116-7.067,5.164 c-2.393,1.938-4.253,4.413-6.406,7.008c-1.6,2.862-3.665,5.785-4.781,9.299c-1.619,3.326-2.2,7.156-3.167,10.926 c-0.745,3.824-0.875,7.798-1.349,11.679c-0.048,7.845,0.256,15.379,1.267,21.801c0.529,6.527,2.186,12.042,2.84,15.864 c0.801,3.835,1.258,6.025,1.258,6.025s-1.178-1.905-3.239-5.239c-1.954-3.435-5.272-7.849-8.185-14.125 c-3.357-6.103-6.488-13.611-9.467-22.058c-1.06-4.358-2.498-8.879-3.294-13.724c-0.561-4.897-1.488-9.948-1.115-15.267 c-0.161-5.31,0.925-10.676,1.912-16.099c1.57-5.253,3.243-10.577,5.804-15.315c2.34-4.842,5.287-9.225,8.23-13.046 c2.962-3.939,6.019-7.162,8.899-10.161c5.748-5.655,10.738-9.788,13.646-12.323c1.664-1.394,2.346-2.147,3.088-2.809 c0.657-0.614,1.006-0.942,1.006-0.942l26.712,41.779C98.923,332.481,98.333,332.731,97.229,333.2z M498.127,730.092 c-2.62,3.263-6.731,8.31-12.304,14.029c-2.971,2.816-6.035,5.868-9.849,8.749c-3.572,2.985-7.915,5.717-12.338,8.28 c-4.56,2.506-9.41,4.656-14.534,6.256c-5.07,1.544-10.333,2.79-15.5,3.21l-7.687,0.549l-7.456-0.401 c-4.906-0.062-9.476-1.369-13.877-2.102c-4.39-0.815-8.371-2.446-12.182-3.54c-3.811-1.028-7.23-2.907-10.392-4.159 c-6.405-2.543-10.993-5.481-14.453-7.3c-3.398-1.872-5.339-2.943-5.339-2.943s2.19,0.337,6.022,0.925 c3.808,0.511,9.367,1.712,15.806,1.842c3.242,0.067,6.567,0.633,10.252,0.291c3.636-0.281,7.43-0.099,11.226-0.778 c3.77-0.754,7.683-0.969,11.313-2.34l5.453-1.673l5.059-2.364c13.357-6.47,20.625-17.868,24.836-27.382 c2.083-4.907,3.576-9.24,4.702-12.63c1.225-3.569,1.925-5.608,1.925-5.608l43.267,24.23 C502.076,725.234,500.639,727.001,498.127,730.092z" />
    							</svg>
							</object>
						</div>
					</a>
				</li>
			</ul>
        	<nav class="main_nav">

        		<?php
						$menuClass = '';
						$primaryNav = wp_nav_menu( array( 'theme_location' => 'primary-menu', 'container' => false, 'fallback_cb' => false, 'menu_class' => $menuClass, 'menu_id' => false, 'echo' => false,'walker' => new My_Walker_Nav_Menu()) );
						

						if ( '' == $primaryNav ) :
					?>

            	<ul class>
            		<?php if ( 'on' == et_get_option( 'divi_home_link' ) ) { ?>

	            		<li class="tab login_tab" <?php if ( is_home() ) echo( 'class="current_page_item"' ); ?>>
	        				<a href="<?php echo esc_url( home_url( '/' ) ); ?>" tabindex="118">
	        					<?php esc_html_e( 'Home', 'Divi' ); ?>
	        				</a>
	        			</li>
        			<?php }; ?>
        		</ul>
        		<?php
        			else :
        				echo( $primaryNav );
        			endif;
        		?>
        	</nav>
	    </div>
	    <div class="end"></div>
	</header>

	<header id="main-header" class="<?php echo esc_attr( $primary_nav_class ); ?>">
			<div class="container clearfix">
			<?php
				$logo = ( $user_logo = et_get_option( 'divi_logo' ) ) && '' != $user_logo
					? $user_logo
					: $template_directory_uri . '/images/logo.png';
			?>
				<a href="<?php echo esc_url( home_url( '/' ) ); ?>">
					<img src="<?php echo esc_attr( $logo ); ?>" alt="<?php echo esc_attr( get_bloginfo( 'name' ) ); ?>" id="logo" />
				</a>

				<div id="et-top-navigation">
					<nav id="top-menu-nav">
					<?php
						$menuClass = 'nav';
						if ( 'on' == et_get_option( 'divi_disable_toptier' ) ) $menuClass .= ' et_disable_top_tier';
						$primaryNav = '';

						$primaryNav = wp_nav_menu( array( 'theme_location' => 'primary-menu', 'container' => '', 'fallback_cb' => '', 'menu_class' => $menuClass, 'menu_id' => 'top-menu', 'echo' => false ) );

						if ( '' == $primaryNav ) :
					?>
						<ul id="top-menu" class="<?php echo esc_attr( $menuClass ); ?>">
							<?php if ( 'on' == et_get_option( 'divi_home_link' ) ) { ?>
								<li <?php if ( is_home() ) echo( 'class="current_page_item"' ); ?>><a href="<?php echo esc_url( home_url( '/' ) ); ?>"><?php esc_html_e( 'Home', 'Divi' ); ?></a></li>
							<?php }; ?>

							<?php show_page_menu( $menuClass, false, false ); ?>
							<?php show_categories_menu( $menuClass, false ); ?>
						</ul>
					<?php
						else :
							echo( $primaryNav );
						endif;
					?>
					</nav>

					<?php
					if ( ! $et_top_info_defined ) {
						et_show_cart_total( array(
							'no_text' => true,
						) );
					}
					?>

					<?php if ( false !== et_get_option( 'show_search_icon', true ) ) : ?>
					<div id="et_top_search">
						<span id="et_search_icon"></span>
						<form role="search" method="get" class="et-search-form et-hidden" action="<?php echo esc_url( home_url( '/' ) ); ?>">
						<?php
							printf( '<input type="search" class="et-search-field" placeholder="%1$s" value="%2$s" name="s" title="%3$s" />',
								esc_attr_x( 'Search &hellip;', 'placeholder', 'Divi' ),
								get_search_query(),
								esc_attr_x( 'Search for:', 'label', 'Divi' )
							);
						?>
						</form>
					</div>
					<?php endif; // true === et_get_option( 'show_search_icon', false ) ?>

					<?php do_action( 'et_header_top' ); ?>
				</div> <!-- #et-top-navigation -->
			</div> <!-- .container -->
		</header> <!-- #main-header -->

	<div id="page-container" style="padding-top: 0px;">

		 
		<div id="et-main-area">