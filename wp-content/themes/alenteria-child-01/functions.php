<?php 
/**
 * Create HTML list of nav menu items.
 * Replacement for the native Walker, using the description.
 *
 * @see    http://wordpress.stackexchange.com/q/14037/
 * @author toscho, http://toscho.de
 */
class My_Walker_Nav_Menu extends Walker_Nav_Menu {  
    function start_lvl(
        &$output, $depth = 0, $args = Array() ) {
             $indent = str_repeat("\t", $depth);     
             $output .= "\n$indent<ul class=\"subnav\">\n";   
    }
}

function renderPhpFile($filename, $vars = null) {
  if (is_array($vars) && !empty($vars)) {
    extract($vars);
  }
  ob_start();
  include $filename;
  return ob_get_clean();
}
 
