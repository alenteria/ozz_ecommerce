<?php
/*
Template Name: Checkout Form
*/

global $wp_query;
if (!isset($wp_query->query_vars['package'])||!in_array($wp_query->query_vars['package'], array('starter','emerging','pro','enterprise')))
{
	header('Location: /pricing');
}

$monthly_subscription = 50;
$packagesArray = array(
		'starter'=>array(
			'name'=>'Starter',
			'description'=>'50 Profiles',
			'price'=>'80'
		),
		'emerging'=>array(
			'name'=>'Emerging',
			'description'=>'100 Profiles',
			'price'=>'160'
		),
		'pro'=>array(
			'name'=>'Pro',
			'description'=>'150 Profiles',
			'price'=>'240'
		),
		'enterprise'=>array(
			'name'=>'Enterprise',
			'description'=>'300 Profiles',
			'price'=>'480'
		)
	);
get_header('checkout');    
?>
<style type="text/css" media="screen">
	html, body{
		background: white;
	}
	.container{
		padding: 0px !important;
		padding-top: 1px !important;
	}
	.container:before{
		background: none !important;
	}	
	input.submit:disabled{
		opacity: 0.3;
	}
	table.subtotal-tb{
		border:1px solid;
		float: right;
		display: block;
		line-height: 15px;
		margin-bottom: -52px;
	}
	table.subtotal-tb tr td,table.subtotal-tb th{
		border: 1px solid;
	}
</style>
<div id="main-content"> 
	<div class="container">
		<div id="content-area" class="clearfix"> 
			<a href="/pricing" style="font-size: 20px; color: #333;"><span>&#8592;&nbsp;&nbsp;Back</span></a>
			<table class='subtotal-tb'>
				<thead>
					<tr>
						<th colspan="2">Price</th>
					</tr>
				</thead>
				<tbody>
					<tr id="package">
						<td ><span>Package </span><span id="name"><?= $packagesArray[$package]['name']; ?></span></td>
						<td class="">$<span id="price"><?= $packagesArray[$package]['price']; ?></span></td>
					</tr>
					<tr id="subscription" style="display:none;">
						<td id="name">Subscription</td>
						<td id="">$<span id="price">0</span></td>
					</tr>
					<tr id="total">
						<td id="total" style="font-weight:bold;">Total</td>
						<td id="">$<span id="price"><?= $packagesArray[$package]['price']; ?></span></td>
					</tr>
				</tbody>
			</table>
			<div class="clearfix"></div>
			<p bt-xtitle="" title="">
				<h4 for="tos" bt-xtitle="" title="">Terms Of Service:</h4><br bt-xtitle="" title="">
				<p  name="tos" id="tos"  rows="6" title="" readonly="readonly" bt-xtitle="Please read our Terms Of Service and then check the box below." style="display: block; height: 100px; overflow: scroll; border: 1px solid rgb(211, 193, 193); padding: 9px; font-size: 12px;"> Knowem.com Premium Commercial Username Signup service offers a simple solution to registering your username across popular sites where unique, branded account names may be created.
				Knowem.com reserves the right to NOT sign up a user if the name could be considered spam or the account will appear to be for reasons other then to be a valuable member of the community and have their name saved. Accounts like "BuyViagra", "FreeAcaiBerry", "FileBankruptcy"
				and "CarAccident" are examples of names that Knowem.com will NOT sign up.  All account names are reviewed before we process them. So please do not waste our time and yours with such sign ups.  Knowem.com reserves the right to deny any account creation for any reason.
				By checking the box below you confirm that the username that you are attempting to signup is your own or the trademarked term or IP is owned by you.  KnowEm staff will do random checks or ask for verification on terms.  We reserve the right to hold any order until proof of legal ownership is found.
				Knowem.com is not responsible if your account is terminated for any reason, this includes but is not limited to: Abuse, Spam and Violations of the sites' Terms of Service or deleted due to community guidelines (i.e. some sites are geared towards the female gender and have been known to delete accounts that are male).
				Due to Facebook requiring a unique cellphone number to capture a vanity URL for a personal Facebook page or at least 25 fans to capture a vanity URL of a fan page we currently do not support Facebook and do not do signups or account creations on this site.
				We strongly suggest after you have been signed up for all sites that you log in and change your password.
				Last modified 3/12/2014.  KnowEm reserves the right to modify these terms of service at any point in the future.
			</p> 
			</p>

			<p bt-xtitle="" title=""> 
				<label for="agreetos" bt-xtitle="" title=""><input id="agreetos" type="checkbox" name="agreetos" value="Y" style="background: none; border: none;" bt-xtitle="" title="" class="bt-active"> Please check this box to state that you have read,<br bt-xtitle="" title="">understand and agree to our Terms of Service.</label>
			</p>

			<p bt-xtitle="" title="">
				<br bt-xtitle="" title=""><label for="subscribeOption" bt-xtitle="" title="">Subscription Option:</label><br bt-xtitle="" title="">
				In addition to the profiles we create on your first purchase, you have the option of enrolling in KnowEm's Brand Management Subscription.  For $59.95 per month,
				KnowEm will create even more account profiles for you on 20 new and popular emerging Social Networks every month.  For serious identity planners,
				this ensures your name will never be brandjacked in the future.<br bt-xtitle="" title=""><br bt-xtitle="" title=""> 
				<label for="subscribeOption" bt-xtitle="" title=""><input id="subscribeOption" type="checkbox" name="subscribeOption" value="y" style="background: none; border: none;" bt-xtitle="" title="">  Yes! Enroll me in the $<?= $monthly_subscription ?> Monthly Brand Management Subscription</label>
			</p>

			<p bt-xtitle="" title="">

				<strong bt-xtitle="" title="">Payments Accepted via Paypal</strong><br bt-xtitle="" title="">
			</p>

			<p bt-xtitle="" title="">

					<form action="https://www.sandbox.paypal.com/cgi-bin/webscr" method="post">
						 <input type="hidden" name="business" value="arnel.lenteria@gmail.com" />
						 <input type="hidden" name="cmd" value="_xclick" />
						 <!-- Instant Payment Notification & Return Page Details -->
						 <input type="hidden" name="notify_url" value="http://www.saveabrand.com/wordpress/?s2member_paypal_notify=1" />
						 <input type="hidden" name="cancel_return" value="http://www.saveabrand.com/" />
						 <input type="hidden" name="return" value="http://www.saveabrand.com/wordpress/?s2member_paypal_return=1" />
						 <input type="hidden" name="rm" value="2" />
						 <!-- Configures Basic Checkout Fields -->
						 <input type="hidden" name="lc" value="" />
						 <input type="hidden" name="no_shipping" value="1" />
						 <input type="hidden" name="no_note" value="1" />
						 <input type="hidden" name="custom" value="www.saveabrand.com" />
						 <input type="hidden" name="currency_code" value="USD" />
						 <input type="hidden" name="page_style" value="paypal" />
						 <input type="hidden" name="charset" value="utf-8" />
						 <input type="hidden" name="item_name" value="<?= $packagesArray[$package]['name'] .'&nbsp;/&nbsp;'. $packagesArray[$package]['description']?>" />
						 <input type="hidden" name="item_number" value="1" />
						 <!-- Configures s2Member's Unique Invoice ID/Code  -->
						 <input type="hidden" name="invoice" value="<?php echo S2MEMBER_VALUE_FOR_PP_INV(); ?>" />
						 <!-- Identifies/Updates An Existing User/Member (when/if applicable)  -->
						 <input type="hidden" name="on0" value="<?php echo S2MEMBER_CURRENT_USER_VALUE_FOR_PP_ON0; ?>" />
						 <input type="hidden" name="os0" value="<?php echo S2MEMBER_CURRENT_USER_VALUE_FOR_PP_OS0; ?>" />
						 <!-- Identifies The Customer's IP Address For Tracking -->
						 <input type="hidden" name="on1" value="<?php echo S2MEMBER_CURRENT_USER_VALUE_FOR_PP_ON1; ?>" />
						 <input type="hidden" name="os1" value="<?php echo S2MEMBER_CURRENT_USER_VALUE_FOR_PP_OS1; ?>" />
						 <!-- Controls Modify Behavior At PayPal Checkout -->
						 <input type="hidden" name="modify" value="0" />
						 <!-- Customizes Prices, Payments & Billing Cycle -->
						 <input type="hidden" name="amount" value="<?= $packagesArray[$package]['price'] ?>" />
						 <input type="hidden" disabled name="src" value="1" />
						 <input type="hidden" disabled name="srt" value="" />
						 <input type="hidden" disabled name="sra" value="1" />
						 <input type="hidden" disabled name="a1" value="<?= $packagesArray[$package]['price']+$monthly_subscription ?>" />
						 <input type="hidden" disabled name="p1" value="1" />
						 <input type="hidden" disabled name="t1" value="M" />
						 <input type="hidden" disabled name="a3" value="<?= $monthly_subscription; ?>" />
						 <input type="hidden" disabled name="p3" value="1" />
						 <input type="hidden" disabled name="t3" value="M" />
						 <!-- Displays The PayPal Image Button -->
						 <input disabled class="submit" type="image" src="https://www.paypal.com/en_US/i/btn/btn_xpressCheckout.gif" style="width:auto; height:auto; border:0;" alt="PayPal" />
						</form>
			</p>
		</div>  
	</div>  
</div>   

<script type="text/javascript">
	var agreed = false;
	var subscribed = false;

	$(document).ready(function(){

		$('#agreetos').change(function(){
			if($(this).is(':checked')){
				agreed = true;

				$('input.submit').prop('disabled', false);
			}else
				$('input.submit').prop('disabled', true);

		});

		$('#subscribeOption').change(function(){
			if($(this).is(':checked')){
				subscribed = true;
				$('input[name="cmd"').val('_xclick-subscriptions');
				$('input[name="src"], input[name="srt"], input[name="sra"], input[name="a1"], input[name="p1"], input[name="t1"], input[name="a3"], input[name="p3"], input[name="t3"]').prop('disabled', false);

				$('input[name="amount"').prop('disabled', true);
				$('#subscription').show();
				$('#subscription #price').html("<?= $monthly_subscription; ?>");
				$('#total #price').html("<?= $packagesArray[$package]['price']+$monthly_subscription; ?>");
			}else{
				$('input[name="cmd"').val('_xclick');
				$('#subscription').hide();
				$('input[name="amount"').prop('disabled', false);

				$('#total #price').html("<?= $packagesArray[$package]['price']; ?>");
				$('input[name="src"], input[name="srt"], input[name="sra"], input[name="a1"], input[name="p1"], input[name="t1"], input[name="a3"], input[name="p3"], input[name="t3"]').prop('disabled', true);
			}
		}); 

	});
</script>