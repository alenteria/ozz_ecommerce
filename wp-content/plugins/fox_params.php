<?php
/* Plugin Name: fox_params
Plugin URI: http://iwanageek.blogspot.com
Description: A plugin to allow parameters to be passed in the URL and recognized by WordPress
Author: Arnel T. Lenteira
Author URI: http://iwanageek.blogspot.com
*/
add_filter('query_vars', 'parameter_queryvars' );
function parameter_queryvars( $qvars )
{
$qvars[] = 'package';
return $qvars;
}
?>